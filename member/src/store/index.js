import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

import state from './state.js'
import getters from './getters.js'
import mutations from './mutations.js'
import actions from './actions.js'

export default new Vuex.Store({
/** 
 * Penulisan ini bisa dipersingkat,
 * karena key dan value memiliki nama yang sama.
 * Mengikuti aturan ES6
 */
    // state = state, 
    // getters = getters,
    // mutations = mutations, 
    // actions = actions

/**
 * Setelah dipersingkat, 
 * menjadi
 */
    state, 
    getters,
    mutations, 
    actions
})





