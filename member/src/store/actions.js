import * as mutations from './mutation-types'

export default {
    showMember (context, member) {
        // manggil mutation
        context.commit(mutations.SHOW_MEMBER, member)
    },
    showMembers (context) {
        // manggil mutation
        context.commit(mutations.SHOW_MEMBERS)
    },
    addMember (context) {
        context.commit(mutations.ADD_MEMBER)
    }, 
    removeMember (context, id) {
        context.commit(mutations.REMOVE_MEMBER, id)

        /** Ketika berhasil menghapus member, maka akan menampilkan semua member */
        // setTimeout(fungsi, waktu)
        setTimeout(() => context.dispatch('showMembers'), 100)
    }

}